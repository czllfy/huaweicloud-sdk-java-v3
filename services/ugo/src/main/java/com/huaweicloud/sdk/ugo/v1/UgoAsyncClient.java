package com.huaweicloud.sdk.ugo.v1;

import com.huaweicloud.sdk.core.ClientBuilder;
import com.huaweicloud.sdk.core.HcClient;
import com.huaweicloud.sdk.core.invoker.AsyncInvoker;
import com.huaweicloud.sdk.ugo.v1.model.*;

import java.util.concurrent.CompletableFuture;

public class UgoAsyncClient {

    protected HcClient hcClient;

    public UgoAsyncClient(HcClient hcClient) {
        this.hcClient = hcClient;
    }

    public static ClientBuilder<UgoAsyncClient> newBuilder() {
        return new ClientBuilder<>(UgoAsyncClient::new);
    }

    /**
     * 查询当前支持的API版本信息列表
     *
     * 查询当前支持的API版本信息列表。
     * 
     * 详细说明请参考华为云API Explorer。
     * Please refer to Huawei cloud API Explorer for details.
     *
     * @param ListApiVersionsRequest 请求对象
     * @return CompletableFuture<ListApiVersionsResponse>
     */
    public CompletableFuture<ListApiVersionsResponse> listApiVersionsAsync(ListApiVersionsRequest request) {
        return hcClient.asyncInvokeHttp(request, UgoMeta.listApiVersions);
    }

    /**
     * 查询当前支持的API版本信息列表
     *
     * 查询当前支持的API版本信息列表。
     * 
     * 详细说明请参考华为云API Explorer。
     * Please refer to Huawei cloud API Explorer for details.
     *
     * @param ListApiVersionsRequest 请求对象
     * @return AsyncInvoker<ListApiVersionsRequest, ListApiVersionsResponse>
     */
    public AsyncInvoker<ListApiVersionsRequest, ListApiVersionsResponse> listApiVersionsAsyncInvoker(
        ListApiVersionsRequest request) {
        return new AsyncInvoker<ListApiVersionsRequest, ListApiVersionsResponse>(request, UgoMeta.listApiVersions,
            hcClient);
    }

    /**
     * 查询指定API版本信息
     *
     * 查询指定API版本信息。
     * 
     * 详细说明请参考华为云API Explorer。
     * Please refer to Huawei cloud API Explorer for details.
     *
     * @param ShowApiVersionRequest 请求对象
     * @return CompletableFuture<ShowApiVersionResponse>
     */
    public CompletableFuture<ShowApiVersionResponse> showApiVersionAsync(ShowApiVersionRequest request) {
        return hcClient.asyncInvokeHttp(request, UgoMeta.showApiVersion);
    }

    /**
     * 查询指定API版本信息
     *
     * 查询指定API版本信息。
     * 
     * 详细说明请参考华为云API Explorer。
     * Please refer to Huawei cloud API Explorer for details.
     *
     * @param ShowApiVersionRequest 请求对象
     * @return AsyncInvoker<ShowApiVersionRequest, ShowApiVersionResponse>
     */
    public AsyncInvoker<ShowApiVersionRequest, ShowApiVersionResponse> showApiVersionAsyncInvoker(
        ShowApiVersionRequest request) {
        return new AsyncInvoker<ShowApiVersionRequest, ShowApiVersionResponse>(request, UgoMeta.showApiVersion,
            hcClient);
    }

    /**
     * SQL翻译接口
     *
     * 实现源库与目标数据之间SQL语句的自动翻译
     * 
     * 详细说明请参考华为云API Explorer。
     * Please refer to Huawei cloud API Explorer for details.
     *
     * @param MigrateSqlStatementRequest 请求对象
     * @return CompletableFuture<MigrateSqlStatementResponse>
     */
    public CompletableFuture<MigrateSqlStatementResponse> migrateSqlStatementAsync(MigrateSqlStatementRequest request) {
        return hcClient.asyncInvokeHttp(request, UgoMeta.migrateSqlStatement);
    }

    /**
     * SQL翻译接口
     *
     * 实现源库与目标数据之间SQL语句的自动翻译
     * 
     * 详细说明请参考华为云API Explorer。
     * Please refer to Huawei cloud API Explorer for details.
     *
     * @param MigrateSqlStatementRequest 请求对象
     * @return AsyncInvoker<MigrateSqlStatementRequest, MigrateSqlStatementResponse>
     */
    public AsyncInvoker<MigrateSqlStatementRequest, MigrateSqlStatementResponse> migrateSqlStatementAsyncInvoker(
        MigrateSqlStatementRequest request) {
        return new AsyncInvoker<MigrateSqlStatementRequest, MigrateSqlStatementResponse>(request,
            UgoMeta.migrateSqlStatement, hcClient);
    }

}
